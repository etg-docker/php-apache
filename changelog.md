# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.0-8.1.1] - 2022-01-03

- initial version

### Added

- php: `8.1.1`
- activated mod_rewrite
- docker image
- test files
- CI/CD for gitlab
