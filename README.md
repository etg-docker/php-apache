# etg-docker: ekleinod/php-apache

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/php-apache/)
- **Docker hub:** [ekleinod/php-apache](https://hub.docker.com/r/ekleinod/php-apache)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/php-apache/), [issue tracker](https://gitlab.com/etg-docker/php-apache/-/issues)


## Supported tags

- `1.0.0`, `1.0.0-8.1.1`, `latest`


## What is this image?

This is a docker image for  php-apache.
The image has the name `ekleinod/php-apache`.
It is based on the excellent [official php:apache image](https://hub.docker.com/_/php) and extends it with the changes needed for my projects.

The changes are:

- activated mod_rewrite


For the complete changelog, see [changelog.md](changelog.md)


## How to use the image

You can use the image as follows:

`docker-compose.yml`

~~~ docker
version: '3.8'
services:

  apache:
    container_name: apache
    image: ekleinod/php-apache
    volumes:
      - ./:/var/www/html
    ports:
      - 8000:80
~~~

Please note, that `/var/www/html` is the directory containing the website.

Start and stop the server with:

~~~ console
$ docker-compose up --detach
$ ...
$ docker-compose down
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Start the server and navigate to <http://localhost:8000>

~~~ console
$ cd test
$ ./up.sh
$ # check working server at http://localhost:8000
$ ./down.sh
~~~


## Releases

The latest release will always be available with:

`ekleinod/php-apache:latest`

There are two naming schemes:

1. `ekleinod/php-apache:<internal>-<php>`

	Example: `ekleinod/php-apache:1.0.0-8.1.1`

2. internal version number `ekleinod/php-apache:<major>.<minor>.<patch>`

	Example: `ekleinod/php-apache:1.0.0`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2022-2022 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
